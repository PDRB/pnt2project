import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router"
import VueX from "vuex"
Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueX)
new Vue({
  render: h => h(App),
}).$mount('#app')
